.. flexfringe documentation master file, created by
   sphinx-quickstart on Mon Dec  7 12:35:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to flexfringe's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Docs
====

.. doxygenclass:: state_merger
   :members: